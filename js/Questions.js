let myQuestions = [{
        question: "1. An enclosed file is a file we can't open anymore ?",
        answers: {
            a: 'True',
            b: 'False'
        },
        correctAnswer: 'b'
    },
    {
        question: "2. A spam is a contraction for Spicy Ham ?",
        answers: {
            a: 'True',
            b: 'False'
        },
        correctAnswer: 'a'
    },
    {
        question: "3. Google is ... ?",
        answers: {
            a: 'a browser',
            b: 'a search engine',
            c: 'an operating system'

        },
        correctAnswer: 'b'
    },
    {
        question: "4. The bluetooth connexion is wireless ?",
        answers: {
            a: 'True',
            b: 'False'
        },
        correctAnswer: 'a'
    },
    {
        question: "5. An array is not ... ?",
        answers: {
            a: 'a table',
            b: 'a set of data',
            c: 'a class'
        },
        correctAnswer: 'c'
    },
    {
        question: "6. The Space Tabulation is the key to write spaces ?",
        answers: {
            a: 'True',
            b: 'False'
        },
        correctAnswer: 'b'
    },
    {
        question: "7. The adress of a website is called html ?",
        answers: {
            a: 'True',
            b: 'False'
        },
        correctAnswer: 'b'
    },
    {
        question: "8. What is a dot ?",
        answers: {
            a: '\"@\"',
            b: '\";\"',
            c: '\".\"',
            d: '\"$\"'
        },
        correctAnswer: 'c'
    },
    {
        question: "9. An advertisement is a way to report error ?",
        answers: {
            a: 'True',
            b: 'False'
        },
        correctAnswer: 'b'
    },
    {
        question: "10. WWW means :",
        answers: {
            a: 'Work Without Wait',
            b: 'World Wide Web',
            c: 'World Weird Web'
        },
        correctAnswer: 'b'
    },
    {
        question: "11. A review is a new css file to improve the website :",
        answers: {
            a: 'True',
            b: 'False'
        },
        correctAnswer: 'b'
    },
    {
        question: "12. What is a nickname ?",
        answers: {
            a: 'a pseudo',
            b: 'the name of a ghost in Harry Potter'
        },
        correctAnswer: 'a'
    },
    {
        question: "13. Adaptive design creates a website in several different layouts, each suited for different screen sizes ?",
        answers: {
            a: 'True',
            b: 'False'
        },
        correctAnswer: 'a'
    },
    {
        question: "14. A CSS class is the same as a JS class ?",
        answers: {
            a: 'True',
            b: 'False'
        },
        correctAnswer: 'b'
    },
    {
        question: "15. A favicon is like a logo, it can be show up everywhere in the website.",
        answers: {
            a: 'True',
            b: 'False'
        },
        correctAnswer: 'b'
    },
    {
        question: "16. What's the meaning of FTP ?",
        answers: {
            a: 'File Transfer Protocol',
            b: 'File Toto Prefers',
            c: 'Finish The Porridge'
        },
        correctAnswer: 'a'
    }
];

let firstPage = document.querySelector('#firstPage');
let firstHeader = document.querySelector('#firstHeader');
let quizPage = document.querySelector('#quizPage');
let quizHeader = document.querySelector('#quizHeader');
quizPage.style.display = 'none';
quizHeader.style.display = 'none';
let startBtn = document.querySelector('#start');
let quizContainer = document.querySelector('#quiz');
let resultsContainer = document.querySelector('#results');
let submitButton = document.querySelector('#submit');

startBtn.addEventListener('click', function() {
    firstPage.style.display = 'none';
    firstHeader.style.display = 'none';
    quizPage.style.display = 'block';
    quizHeader.style.display = 'block';

    generateQuiz(myQuestions, quizContainer, resultsContainer, submitButton);

});
generateQuiz(myQuestions, quizContainer, resultsContainer, submitButton);

function generateQuiz(questions, quizContainer, resultsContainer, submitButton) {

    function showQuestions(questions, quizContainer) {
        // we'll need a place to store the output and the answer choices
        let output = [];
        let answers;

        // for each question...
        for (let i = 0; i < questions.length; i++) {

            // first reset the list of answers
            answers = [];

            // for each available answer...
            for (letter in questions[i].answers) {

                // ...add an html radio button
                answers.push(
                    '<label>' +
                    '<input type="radio" name="question' + i + '" value="' + letter + '">' +
                    letter + ': ' +
                    questions[i].answers[letter] +
                    '</label>'
                );
            }

            // add this question and its answers to the output
            output.push(
                '<div class="question">' + questions[i].question + '</div>' +
                '<div class="answers">' + answers.join(' ') + '</div>'
            );
        }

        // finally combine our output list into one string of html and put it on the page
        quizContainer.innerHTML = output.join('');
    }


    function showResults(questions, quizContainer, resultsContainer) {

        // gather answer containers from our quiz
        let answerContainers = quizContainer.querySelectorAll('.answers');

        // keep track of user's answers
        let userAnswer = '';
        let numCorrect = 0;

        // for each question...
        for (let i = 0; i < questions.length; i++) {

            // find selected answer
            userAnswer = (answerContainers[i].querySelector('input[name=question' + i + ']:checked') || {}).value;

            // if answer is correct
            if (userAnswer === questions[i].correctAnswer) {
                // add to the number of correct answers
                numCorrect++;

                // color the answers green
                answerContainers[i].style.color = 'lightgreen';
            }
            // if answer is wrong or blank
            else {
                // color the answers red
                answerContainers[i].style.color = 'red';
            }
        }

        // show number of correct answers out of total
        resultsContainer.innerHTML = numCorrect + ' out of ' + questions.length;

        // show average score ?
    }

    // show questions right away
    showQuestions(questions, quizContainer);

    // on submit, show results
    submitButton.onclick = function() {
        showResults(questions, quizContainer, resultsContainer);
    }

}